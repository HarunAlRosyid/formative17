package com.nexsoft.arisan.model;

public class MemberModel {
	private int id;
	private String name;
	public String number;
	private int winOrder;
	private int received;
	
	public static int ai=1;
	
	public MemberModel() {
		
	}
	
	public MemberModel(String name, String number,int winOrder,int received) {
		setId(ai++);
		this.name = name;
		this.number = number;
		this.winOrder = winOrder;
		this.received = received;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getWinOrder() {
		return winOrder;
	}

	public void setWinOrder(int winOrder) {
		this.winOrder = winOrder;
	}

	public int getReceived() {
		return received;
	}

	public void setReceived(int received) {
		this.received = received;
	}

	public static int getAi() {
		return ai;
	}

	public static void setAi(int ai) {
		MemberModel.ai = ai;
	}
}
