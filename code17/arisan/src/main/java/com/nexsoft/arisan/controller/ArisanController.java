package com.nexsoft.arisan.controller;

import com.nexsoft.arisan.model.LotteryModel;
import com.nexsoft.arisan.model.MemberModel;
import com.nexsoft.arisan.view.ArisanView;

import java.util.ArrayList;
import java.util.Scanner;

public class ArisanController {
	private MemberModel member;
	private ArisanView view;
	public int paymentPerMember = 150000;
	RandomCodeController random = new RandomCodeController() ;
	
	ArrayList<MemberModel> dataMember = new ArrayList<MemberModel>();
	
	
	public void data(MemberModel data) {
        this.dataMember.add(data);
	}
	
	public ArisanController(MemberModel member, ArisanView view) {
		this.member = member;
		this.view = view;
	}
	
	Scanner input = new Scanner(System.in);

	
	public void chooseMenu() {
		view.listMenu();	
		int meetCount=0;
		
		try {
			int menu = input.nextInt();
			while (menu != 4) {
	            switch (menu) {
	                case 1:
	                	if(isMaximum() == false) {
	                		add();
	                	}else if (isMaximum() == true) {
	                		System.out.println("Peserta arisan sudah penuh");
	                	}
	                	
	                    break;
	                case 2:
	                	if(isEmpty() == true) {
	                		System.out.println("Daftar Peserta Kosong");
	                	} else if(isEmpty() == false) {
	                		showAll();
	                	}
	                    break;
	                case 3:
	                	if(random.lotteryIsEmpty() == false) {
	                		if(random.lotteryMaximum()==true){
			                	meetCount= meetCount+1;
			                	lottery(meetCount);
		                	} else if(random.lotteryMaximum()==false){
		                		System.out.println("Slot belum penuh");
			                	}
	                	} else if(random.lotteryIsEmpty() == true) {
	                		System.out.println("Nomer undian Kosong");
	                	}

	                    break;
	                case 4:
	                    System.exit(0);
	                    break;
	                default:
	                   System.out.println("Pilih Menu 1-4");
	            }
	            view.listMenu();	
	            menu = input.nextInt();
	        }
			
		}catch(Exception e) {
			System.out.println("Pilih menu ,hanya dengan angka 1-4");
			System.out.println("Sitem Berakhir");
			System.exit(0);
		}
	}
	
	public void add() {
		String numberMember = random.randomGenerator();
		
		view.breakln();
		view.formAdd();
		String name= input.next();

		data(new MemberModel(name, numberMember,0,0));
		random.dataLottery(new LotteryModel(numberMember));
	}
	
	public void showAll() {
		view.printAll(dataMember);
	}
	
	public int amounPerMember(int paymentPerMember) {
		return paymentPerMember;
	}
	
	public int lottery(int meetCount) {
		int sumReceived = countMember()*amounPerMember(paymentPerMember);
	
		view.meet();
		for(int i=0; i<1; i++){
			System.out.println(
				"Pemenang Pertemuan ke-"+meetCount+" dengan nomor:" +
						random.getRandomLotteryCode());
			
			updateDataArisan(random.getRandomLotteryCode(), meetCount,sumReceived);
			random.remove();
		}
		return meetCount;
	}
	
	
	public void updateDataArisan(String code, int meet, int sumReceived) {
	
		for(MemberModel member : dataMember){
			if(member.getNumber().contains(code) == true){
				System.out.println("Atas Nama : "+ member.getName());	
			    	member.setWinOrder(meet);
			    	member.setReceived(sumReceived);
			} else if(member.getNumber().contains(code)) {
				 System.out.println("Data tidak ditemukan");
			}
		}
	}
	
	public int countMember() {
		int count = dataMember.size();
		return count;
	}
	
	public boolean isEmpty(){
        return dataMember.isEmpty();
    }
	
	
	public boolean isMaximum(){
        return dataMember.size()>=15 ;
    }
}
