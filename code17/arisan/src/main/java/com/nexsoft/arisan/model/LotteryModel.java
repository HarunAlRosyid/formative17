package com.nexsoft.arisan.model;

public class LotteryModel {
	private String lotteryCode;
	
	public LotteryModel(String lotteryCode) {
		this.lotteryCode = lotteryCode;
	}

	public String getLotteryCode() {
		return lotteryCode;
	}

	public void setLotteryCode(String lotteryCode) {
		this.lotteryCode = lotteryCode;
	}
}
