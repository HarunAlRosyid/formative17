package com.nexsoft.arisan;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.nexsoft.arisan.controller.ArisanController;
import com.nexsoft.arisan.controller.RandomCodeController;
import com.nexsoft.arisan.model.LotteryModel;
import com.nexsoft.arisan.model.MemberModel;
import com.nexsoft.arisan.view.ArisanView;

class ArisanControllerTest {
	ArisanView view= new ArisanView();
	MemberModel member= new MemberModel();
	ArisanController controller= new ArisanController(member,view);
	RandomCodeController random= new RandomCodeController();

	@BeforeEach
	@DisplayName("set data")
	void data() {
		controller.data(new MemberModel("ss","sksks",0,0));
		random.dataLottery(new LotteryModel("ssskss89"));
	}

	@Test
	void testCountSizeMember() {
		assertEquals(1,controller.countMember());
	}
	@Test
	void testAmounPerMemberTrue() {
		int amount = controller.amounPerMember(150000);
		assertTrues(amount == 1500000);
	}
	@Test
	void testAmounPerMember() {
		int amount = controller.amounPerMember(1500000);
		assertEquals( 1500000,amount);
	}
	@Test
	void testEmptyMember() {
		boolean empty = controller.isEmpty();
		assertTrues(empty == true);
	}
	@Test
	void testEmptyMembers() {
		boolean empty = controller.isEmpty();
		assertFalse(empty == true);
	}
	@Test
	void testEmptyCode() {
		boolean empty = random.lotteryIsEmpty();
		assertFalse(empty == true);
	}
	
	@Test
	void testEmptyCodes() {
		boolean empty = random.lotteryIsEmpty();
		assertTrue(empty == false);
	}
	
	@Test
	void testMaximumMember() {
		boolean max = controller.isMaximum();
		assertTrues(max == true);
	}

	@Test
	void testLottery() {
		assertEquals(1,controller.lottery(0));
	}
	@Test
	void testRandomLottery() {
		assertEquals(1,random.randomLottery());
	}
	@Test
	void testGetRandomLotteryCode() {
		assertEquals("skssj",random.getRandomLotteryCode());
	}
	@Test
	void testGetName() {
		assertEquals("mm",member.getName());
	}
	@Test
	void testCurrency() {
		String currency = view.currencyID(10.0);
		assertEquals("10000.00",currency);
	}
	
	private void assertTrues(boolean b) {
		// TODO Auto-generated method stub
		
	}

	
}
